/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#define _GNU_SOURCE // for pthread_tryjoin_np
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <modal_start_stop.h>
#include <modal_pipe_server.h>

#include "modal_lepton_cci.h"
#include "common.h"
#include "publisher.h"
#include "color_map.h"
#include "config_file.h"


#define CH_RAW                  0
#define CH_CLR                  1
#define CH_16RAW                2


#define PIPE_NAME_RAW           "lepton0_raw"
#define PIPE_NAME_CLR           "lepton0_color"
#define PIPE_NAME_16RAW         "lepton0_16raw"
#define PIPE_NAME_LOG           "lepton0_log"
#define PIPE_NAME_TIMING           "lepton0_timing"

static uint8_t* raw_frame_for_publish;
static uint8_t* frame;
static uint8_t* frame_clr;
static uint16_t* frame_raw16;
static int initialized;
static int64_t ts_for_publish = 0;
static pthread_t thread;
static int frame_ctr = 0;
static int server_pipes_are_open = 0;


// Convert raw packet data to valid frame
static void _raw_to_greyscale(uint8_t *raw_frame, uint8_t *frame)
{
    int min = 65535;
    int max = 0;

    uint16_t* frame_buffer = (uint16_t*)raw_frame;
    int idx;
    for(idx=0; idx<RAW_FRAME_SIZE_16; idx++){
        // First 2 words contain header information, not relevant to frame
        if (idx % (VOSPI_PACKET_SIZE_16) < 2)
            continue;

        //flip the MSB and LSB at the last second
        int temp = raw_frame[idx*2];
        raw_frame[idx*2] = raw_frame[idx*2+1];
        raw_frame[idx*2+1] = temp;

        uint16_t value = frame_buffer[idx];

        if(value > max) {
            max = value;
        }
        if(value < min) {
            min = value;
        }
    }
    //printf("min: %3d  max: %3d", min, max);
    float diff = max - min;
    float scale = 255/diff;

    int frame_idx = en_rotate ? FRAME_SIZE-1 : 0;
    for(idx=0; idx<RAW_FRAME_SIZE_16; idx++){
        // First 2 words contain header information, not relevant to frame
        if (idx % (VOSPI_PACKET_SIZE_16) < 2)
            continue;

        uint16_t value = frame_buffer[idx];
        frame[frame_idx] = (value - min) * scale;
        frame_raw16[frame_idx] = value;

        frame_idx+=(en_rotate ? -1 : 1);


    }

}


static void _grey_to_color(uint8_t *raw_frame, uint8_t *color_frame)
{
    int i;
    for(i=0; i<FRAME_SIZE; i++){
        // Use colormap to generate RGB888 image
        uint8_t value = raw_frame[i];
        color_frame[3 * i]     = color_map[3 * value];
        color_frame[3 * i + 1] = color_map[3 * value + 1];
        color_frame[3 * i + 2] = color_map[3 * value + 2];
    }
    return;
}


static void connect_handler(int ch, int client_id, char* client_name, __attribute__((unused)) void* context)
{
    printf("client \"%s\" connected to channel %d  with client id %d\n", client_name, ch, client_id);
    return;
}


static void disconnect_handler(int ch, int client_id, char* name, __attribute__((unused)) void* context)
{
    printf("client \"%s\" with id %d has disconnected from channel %d\n", name, client_id, ch);
    return;
}


#ifdef PLATFORM_QRB5165
static void control_pipe_handler(int ch, char* string, int bytes, __attribute__((unused)) void* context)
{
    if(strncmp(string, COMMAND_SET_FFC_SHUTTER_AUTO, strlen(COMMAND_SET_FFC_SHUTTER_AUTO))==0){
        printf("received command to set ffc shutter mode to auto\n");
        if(!en_i2c){
            fprintf(stderr, "WARNING, can't set ffc shutter mode unless I2C is enabled\n");
        }
        modal_lepton_set_ffc_auto();
        return;
    }
    if(strncmp(string, COMMAND_SET_FFC_SHUTTER_MANUAL, strlen(COMMAND_SET_FFC_SHUTTER_MANUAL))==0){
        printf("received command to set ffc shutter mode to manual\n");
        if(!en_i2c){
            fprintf(stderr, "WARNING, can't set ffc shutter mode unless I2C is enabled\n");
        }
        modal_lepton_set_ffc_manual();
        return;
    }
    if(strncmp(string, COMMAND_SET_FFC_SHUTTER_FLOW, strlen(COMMAND_SET_FFC_SHUTTER_FLOW))==0){
        printf("received command to set ffc shutter mode to flow\n");
        if(!en_i2c){
            fprintf(stderr, "WARNING, can't set ffc shutter mode unless I2C is enabled\n");
        }
        modal_lepton_set_ffc_flow();
        return;
    }
    if(strncmp(string, COMMAND_RUN_FFC_SHUTTER, strlen(COMMAND_RUN_FFC_SHUTTER))==0){
        printf("received command to run ffc shutter\n");
        if(!en_i2c){
            fprintf(stderr, "WARNING, can't control ffc shutter unless I2C is enabled\n");
        }
        modal_lepton_run_ffc();
        return;
    }
    if(strncmp(string, COMMAND_REBOOT_LEPTON, strlen(COMMAND_REBOOT_LEPTON))==0){
        printf("received command to run reboot lepton\n");
        if(!en_i2c){
            fprintf(stderr, "WARNING, can't reboot lepton unless I2C is enabled\n");
        }
        modal_lepton_reboot();
        return;
    }

    printf("WARNING: Server received unknown command through the control pipe!\n");
    printf("got %d bytes. Command is: %s on ch %d\n", bytes, string, ch);
    return;
}
#endif


static void _create_server_pipes(void)
{
    if(server_pipes_are_open) return;

    printf("creating MPA server pipes\n");

    int pipe_server_flags = 0;

#ifdef PLATFORM_QRB5165
    if(en_i2c){
        printf("enabling MPA control interface\n");
        pipe_server_flags |= SERVER_FLAG_EN_CONTROL_PIPE;
        pipe_server_set_control_cb(CH_RAW,   &control_pipe_handler, NULL);
        pipe_server_set_control_cb(CH_CLR,   &control_pipe_handler, NULL);
        pipe_server_set_control_cb(CH_16RAW, &control_pipe_handler, NULL);
    }
#endif


    // set up the 8-bit raw pipe
    pipe_info_t info0 = { \
        .name        = PIPE_NAME_RAW,\
        .location    = PIPE_NAME_RAW,\
        .type        = "camera_image_metadata_t",\
        .server_name = PROCESS_NAME,\
        .size_bytes  = (1024*1024)
    };

    pipe_server_set_connect_cb(CH_RAW, &connect_handler, NULL);
    pipe_server_set_disconnect_cb(CH_RAW, &disconnect_handler, NULL);
    pipe_server_create(CH_RAW, info0, pipe_server_flags);

    cJSON* json = pipe_server_get_info_json_ptr(CH_RAW);
    if(json == NULL){
        fprintf(stderr, "WARNING got NULL pointer in %s\n", __FUNCTION__);
    }
    else{
        cJSON_AddStringToObject(json, "string_format", pipe_image_format_to_string(IMAGE_FORMAT_RAW8));
        cJSON_AddNumberToObject(json, "int_format", IMAGE_FORMAT_RAW8);
        cJSON_AddNumberToObject(json, "width", FRAME_WIDTH);
        cJSON_AddNumberToObject(json, "height", FRAME_HEIGHT);
        cJSON_AddNumberToObject(json, "framerate", 9);
        pipe_server_update_info(CH_RAW);
    }


    // set up the rgb pipe
    pipe_info_t info1 = { \
        .name        = PIPE_NAME_CLR,\
        .location    = PIPE_NAME_CLR,\
        .type        = "camera_image_metadata_t",\
        .server_name = PROCESS_NAME,\
        .size_bytes  = (1024*1024)
    };

    pipe_server_set_connect_cb(CH_CLR, &connect_handler, NULL);
    pipe_server_set_disconnect_cb(CH_CLR, &disconnect_handler, NULL);
    pipe_server_create(CH_CLR, info1, pipe_server_flags);

    json = pipe_server_get_info_json_ptr(CH_CLR);
    if(json == NULL){
        fprintf(stderr, "WARNING got NULL pointer in %s\n", __FUNCTION__);
    }
    else{
        cJSON_AddStringToObject(json, "string_format", pipe_image_format_to_string(IMAGE_FORMAT_RGB));
        cJSON_AddNumberToObject(json, "int_format", IMAGE_FORMAT_RGB);
        cJSON_AddNumberToObject(json, "width", FRAME_WIDTH);
        cJSON_AddNumberToObject(json, "height", FRAME_HEIGHT);
        cJSON_AddNumberToObject(json, "framerate", 9);
        pipe_server_update_info(CH_CLR);
    }


    // set up the raw 16 pipe
    pipe_info_t info2 = { \
        .name        = PIPE_NAME_16RAW,\
        .location    = PIPE_NAME_16RAW,\
        .type        = "camera_image_metadata_t",\
        .server_name = PROCESS_NAME,\
        .size_bytes  = (1024*1024)
    };

    pipe_server_set_connect_cb(CH_16RAW, &connect_handler, NULL);
    pipe_server_set_disconnect_cb(CH_16RAW, &disconnect_handler, NULL);
    pipe_server_create(CH_16RAW, info2, pipe_server_flags);

    json = pipe_server_get_info_json_ptr(CH_16RAW);
    if(json == NULL){
        fprintf(stderr, "WARNING got NULL pointer in %s\n", __FUNCTION__);
    }
    else{
        cJSON_AddStringToObject(json, "string_format", pipe_image_format_to_string(IMAGE_FORMAT_RAW16));
        cJSON_AddNumberToObject(json, "int_format", IMAGE_FORMAT_RAW16);
        cJSON_AddNumberToObject(json, "width", FRAME_WIDTH);
        cJSON_AddNumberToObject(json, "height", FRAME_HEIGHT);
        cJSON_AddNumberToObject(json, "framerate", 9);
        pipe_server_update_info(CH_16RAW);
    }

    // init log pipe
    pipe_info_t info3 = {
        PIPE_NAME_LOG,        // name
        PIPE_NAME_LOG,    // location
        "lepton_log_packet",  // type
        PROCESS_NAME,               // server_name
        (1024*1024),              // size_bytes
        0                           // server_pid
    };

    pipe_server_create(CH_LOG, info3, pipe_server_flags); //do we need to add json and connect and disconnect cb?


    if (en_timing_msg) {
        // init timing pipe
        pipe_info_t info4 = {
            PIPE_NAME_TIMING,        // name
            PIPE_NAME_TIMING,    // location
            "lepton_timing_packet",  // type
            PROCESS_NAME,               // server_name
            (1024*1024),              // size_bytes
            0                           // server_pid
        };

        pipe_server_create(CH_TIMING, info4, pipe_server_flags); //do we need to add json and connect and disconnect cb?
    }

#ifdef PLATFORM_QRB5165
    if(en_i2c){
        pipe_server_set_available_control_commands(CH_RAW,   CONTROL_COMMANDS);
        pipe_server_set_available_control_commands(CH_CLR,   CONTROL_COMMANDS);
        pipe_server_set_available_control_commands(CH_16RAW, CONTROL_COMMANDS);
    }
#endif
    server_pipes_are_open = 1;
    return;
}



// Process raw frames and send to pipe
static void* _publish_thread_func(__attribute__((unused))void* context)
{
    if(!server_pipes_are_open){
        _create_server_pipes();
    }

    // Initialize camera metadata structs
    camera_image_metadata_t meta_raw = { 0 };
    meta_raw.size_bytes   = FRAME_SIZE;
    meta_raw.format       = IMAGE_FORMAT_RAW8;
    meta_raw.exposure_ns  = 0;
    meta_raw.gain         = 0.0;
    meta_raw.magic_number = CAMERA_MAGIC_NUMBER;
    meta_raw.width        = FRAME_WIDTH;
    meta_raw.height       = FRAME_HEIGHT;
    meta_raw.stride       = FRAME_WIDTH;
    meta_raw.timestamp_ns = ts_for_publish;
    meta_raw.frame_id     = frame_ctr;

    camera_image_metadata_t meta_clr = meta_raw;
    meta_clr.size_bytes   = FRAME_SIZE * 3;
    meta_clr.stride       = FRAME_WIDTH * 3;
    meta_clr.format       = IMAGE_FORMAT_RGB;

    camera_image_metadata_t meta_16raw = meta_raw;
    meta_16raw.size_bytes   = FRAME_SIZE * 2;
    meta_16raw.stride       = FRAME_WIDTH * 2;
    meta_16raw.format       = IMAGE_FORMAT_RAW16;

    int n_raw_clients   = pipe_server_get_num_clients(CH_RAW);
    int n_clr_clients   = pipe_server_get_num_clients(CH_CLR);
    int n_raw16_clients = pipe_server_get_num_clients(CH_16RAW);

    if(n_raw_clients<1 && n_clr_clients<1 && n_raw16_clients<1) return NULL;

    // unpack raw SPI data to an 8-bit grey image, this also swaps LSB/MSB
    // in the original data for the raw16 channel to publish.
    _raw_to_greyscale(raw_frame_for_publish, frame);

    if(n_raw_clients>=1){
        pipe_server_write_camera_frame(CH_RAW, meta_raw, frame);
    }
    if(n_clr_clients>=1){
        _grey_to_color(frame, frame_clr);
        pipe_server_write_camera_frame(CH_CLR, meta_clr, frame_clr);
    }
    if(n_raw16_clients>=1){
        pipe_server_write_camera_frame(CH_16RAW, meta_16raw, frame_raw16);
    }

    return NULL;
}


void publish_async(char* new_raw_frame, int64_t new_ts)
{
    // really make sure the thread has stopped before we start it again
    if(thread!=0 && pthread_tryjoin_np(thread, NULL)){
        fprintf(stderr, "WARNING, publisher falling behind\n");
        return;
    }
    if(!initialized){
        fprintf(stderr, "WARNING, publisher not initialized\n");
        return;
    }

    // TODO multiple buffers to remove this memcopy
    memcpy(raw_frame_for_publish, new_raw_frame, RAW_FRAME_SIZE);
    frame_ctr++;
    ts_for_publish = new_ts;

    pthread_create(&thread, NULL, _publish_thread_func, NULL);
    return;
}


void init_publisher(void)
{
    raw_frame_for_publish = (uint8_t*)malloc(RAW_FRAME_SIZE);
    frame = (uint8_t*)malloc(FRAME_SIZE);
    frame_clr = (uint8_t*)malloc(FRAME_SIZE*3);
    frame_raw16 = (uint16_t*)malloc(FRAME_SIZE*sizeof(uint16_t));
    initialized = 1;
    // don't create server pipes yet, not until the sensor driver is running
    return;
}
