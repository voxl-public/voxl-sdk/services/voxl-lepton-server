/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <stdio.h>
#include <modal_json.h>
#include "config_file.h"

#define CONF_FILE "/etc/modalai/voxl-lepton-server.conf"





#ifdef PLATFORM_QRB5165
	int spi_bus = 14;
	int spi_speed = 16000000;
#else
	int spi_bus = 9;
	int spi_speed = 20000000;
#endif

int en_i2c = 0;
int i2c_bus = 0;
int flow_shutter_s = 20;
int en_rotate = 0;
int en_timing_msg = 0;
int assign_cpu_num = 7;


// default to normal automatic shutter mode. This can get set to manual in the
// config file for more specific use cases like optic flow where manual control
// over the shutter is important
shutter_mode_t shutter_mode = SHUTTER_MODE_AUTO;

// not config file entries
int en_debug = 0;
int en_debug_verbose = 0;
int en_debug_timing = 0;
int en_debug_ffc = 0;

int closePeriodInFramesInAir = 1;
int openPeriodInFramesInAir = 0;
int desiredFfcPeriodMsInAir = 360000; // 6 minutes
int desiredFfcTempDeltaCentiDegInAir = 600; // 6 degrees



#define CONFIG_FILE_HEADER "\
/**\n\
 * VOXL FLIR Server Configuration File\n\
 *\n\
 * Auto Shutter mode is the factory default for FLIR Lepton. It will automatically\n\
 * run the FFC shutter every 3 minutes or during large sensor temp swings.\n\
 * Manual Shutter mode disables the shutter unless manually commanded via\n\
 * the control pipe or the voxl-flir-shutter utility.\n\
 * Flow shutter mode is specific to using the FLIR for optical flow position hold.\n\
 *    It will run the shutter every flow_shutter_s on the ground and while flying\n\
 *    in manual or acro mode. In position mode it lets the Lepton decide when to\n\
 *    do the shutter but with updated parameters.\n\
 *\n\
 * QRB5165 available settings:\n\
 *\n\
 * spi_bus:        default 14\n\
 * spi_speed:      default 16000000\n\
 * en_i2c:         enable optional I2C CCI\n\
 * i2c_bus:        default 0 for use with M130 expansion board\n\
 * shutter_mode:   auto, manual, or flow\n\
 * assign_cpu_num: assign to certain cpu number, -1 to disable\n\
 * en_timing_msg:  enable debug timing message\n\
 * en_rotate:      rotate image 180 to compensate for being mounted upside down\n\
 *\n\
 * flow_shutter_s: default 30, seconds to wait between shutters in flow mode while on the ground\n\
 * closePeriodInFramesInAir:  Lepton usually does 4, reduce to default 1 for fast shutter in air\n\
 * openPeriodInFramesInAir:   Lepton usually does 1, reduce to default 0 for faster shutter in air\n\
 * desiredFfcPeriodMsInAir:   Lepton usually does 3 minutes default increases this to 6 minutes in the air\n\
 * desiredFfcTempDeltaCentiDegInAir: Lepton usually does 1.5C, increase to 4.0 (400 centiDeg)\n\
 *\n\
 * APQ8096 not supported.\n\
 *\n\
**/"



void config_file_print(void)
{
	printf("=================================================================\n");
	printf("spi_bus:         %d\n", spi_bus);
	printf("spi_speed:       %d\n", spi_speed);
#ifdef PLATFORM_QRB5165
	const char* shutter_mode_strings[] = SHUTTER_MODE_STRINGS;
	printf("en_i2c:          %d\n", en_i2c);
	printf("i2c_bus:         %d\n", i2c_bus);
	printf("shutter_mode:    %s\n", shutter_mode_strings[shutter_mode]);
	printf("flow_shutter_s:  %d\n", flow_shutter_s);
	printf("closePeriodInFramesInAir:         %d\n", closePeriodInFramesInAir);
	printf("openPeriodInFramesInAir:          %d\n", openPeriodInFramesInAir);
	printf("desiredFfcPeriodMsInAir:          %d\n", desiredFfcPeriodMsInAir);
	printf("desiredFfcTempDeltaCentiDegInAir: %d\n", desiredFfcTempDeltaCentiDegInAir);
	printf("assign_cpu_num:  %d\n", assign_cpu_num);
#endif
	printf("en_timing_msg:   %d\n", en_timing_msg);
	printf("en_rotate:       %d\n", en_rotate);
	printf("=================================================================\n");
	return;
}



int config_file_load(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;



	json_fetch_int_with_default(parent, "spi_bus", &spi_bus, spi_bus);
	json_fetch_int_with_default(parent, "spi_speed", &spi_speed, spi_speed);

#ifdef PLATFORM_QRB5165
	const char* shutter_mode_strings[] = SHUTTER_MODE_STRINGS;
	const int n_modes = sizeof(shutter_mode_strings)/sizeof(shutter_mode_strings[0]);
	json_fetch_bool_with_default(parent, "en_i2c", &en_i2c, en_i2c);
	json_fetch_int_with_default(parent, "i2c_bus", &i2c_bus, i2c_bus);
	json_fetch_enum_with_default(parent, "shutter_mode", (int*)&shutter_mode, shutter_mode_strings, n_modes, shutter_mode);
	json_fetch_int_with_default(parent, "flow_shutter_s", &flow_shutter_s, flow_shutter_s);
	json_fetch_int_with_default(parent, "closePeriodInFramesInAir", &closePeriodInFramesInAir, closePeriodInFramesInAir);
	json_fetch_int_with_default(parent, "openPeriodInFramesInAir", &openPeriodInFramesInAir, openPeriodInFramesInAir);
	json_fetch_int_with_default(parent, "desiredFfcPeriodMsInAir", &desiredFfcPeriodMsInAir, desiredFfcPeriodMsInAir);
	json_fetch_int_with_default(parent, "desiredFfcTempDeltaCentiDegInAir", &desiredFfcTempDeltaCentiDegInAir, desiredFfcTempDeltaCentiDegInAir);
	json_fetch_int_with_default(parent, "assign_cpu_num", &assign_cpu_num, assign_cpu_num);
#endif

	json_fetch_bool_with_default(parent, "en_timing_msg", &en_timing_msg, en_timing_msg);
	json_fetch_bool_with_default(parent, "en_rotate", &en_rotate, en_rotate);

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);



	return 0;
}
