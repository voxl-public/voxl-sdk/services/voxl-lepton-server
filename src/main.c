/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <stdbool.h>
#include <string.h>
#include <sched.h>

#include <c_library_v2/common/mavlink.h>
#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include <rc_math/other.h>

#include "common.h"
#include "config_file.h"
#include "publisher.h"
#include "parser.h"
#include "autopilot_monitor.h"
#include "modal_lepton_cci.h"
#include "io_expander.h"

struct lepton_log_packet lepton_log;


static void _quit(int ret)
{

    printf("Starting shutdown sequence\n");
    stop_spi_parser();
    pipe_server_close_all();
    remove_pid_file(PROCESS_NAME);
    if(ret==0) printf("Exiting Cleanly\n");
    exit(ret);
    return;
}


static void _print_usage(void)
{
    printf("Usage: voxl-lepton-server <options>\n");
    printf("Options:\n");
    printf("-c, --config      load/create config file only, used by configurator\n");
    printf("-d, --debug       Show debug messages.\n");
    printf("-e, --debug_ffc   debug messages for shutter and CCI\n");
    printf("-h, --help        Show help.\n");
    printf("-t, --timing      Enable timing debug print messages\n");
    printf("-v, --verbose     Enable verbose messages\n");
}


static int _parse_opts(int argc, char* argv[])
{
    static struct option long_options[] =
    {
        {"config",                no_argument,       0, 'c'},
        {"debug",                 no_argument,       0, 'd'},
        {"debug_ffc",             no_argument,       0, 'e'},
        {"timing",                no_argument,       0, 't'},
        {"help",                  no_argument,       0, 'h'},
        {"verbose",               no_argument,       0, 'v'},
        {0, 0, 0, 0}
    };

    while(1){
        int option_index = 0;
        int c = getopt_long(argc, argv, "cdehtv", long_options, &option_index);

        if(c == -1) break; // Detect the end of the options.

        switch(c){

        case 'c':
            config_file_load();
            exit(0);
            break;

        case 'd':
            en_debug = 1;
            break;

        case 'e':
            en_debug_ffc = 1;
            break;

         case 'h':
            _print_usage();
            return -1;

        case 'f':
            en_rotate = 1;
            break;

        case 't':
            en_debug_timing = 1;
            break;

        case 'v':
            en_debug = 1;
            en_debug_verbose = 1;
            break;

        default:
            _print_usage();
            return -1;
        }
    }

    return 0;
}

#ifdef PLATFORM_QRB5165
// for qrb5165 only (right now) set the thread to run on
// assign_cpu_num core
static void _check_and_set_affinity(void)
{
	// only do this once
	static int has_set = 0;
	if(has_set) return;

	cpu_set_t cpuset;
	pthread_t thread;
	thread = pthread_self();

	/* Set affinity mask to include assign_cpu_num only */
	CPU_ZERO(&cpuset);
	CPU_SET(assign_cpu_num, &cpuset);
	if(pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset)){
		perror("pthread_setaffinity_np");
	}

	/* Check the actual affinity mask assigned to the thread */
	if(pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset)){
		perror("pthread_getaffinity_np");
	}
	printf("thread is now locked to the following cores:");
	for (int j = 0; j < CPU_SETSIZE; j++){
		if(CPU_ISSET(j, &cpuset)) printf(" %d", j);
	}
	printf("\n");

	// only do this once on start
	has_set = 1;

	return;
}
#endif


int main(int argc, char* argv[])
{
    global_reboot_cnt = 0;
    global_sync_cnt = 0;

    // parse opts first
    if(_parse_opts(argc, argv)) return -1;

    if(config_file_load()) return -1;
    config_file_print();

    // make sure another instance isn't running
    // if return value is -3 then a background process is running with
    // higher privaledges and we couldn't kill it, in which case we should
    // not continue or there may be hardware conflicts. If it returned -4
    // then there was an invalid argument that needs to be fixed.
    if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

    // start signal handler so we can exit cleanly
    if(enable_signal_handler()==-1){
        fprintf(stderr,"ERROR: failed to start signal handler\n");
        _quit(-1);
    }

    // make PID file to indicate your project is running
    // due to the check made on the call to rc_kill_existing_process() above
    // we can be fairly confident there is no PID file already and we can
    // make our own safely.
    make_pid_file(PROCESS_NAME);

    // set this time-sensitive process to use FIFO scheduler with maximum priority
    pipe_set_process_priority(99);

#ifdef PLATFORM_QRB5165
    if (assign_cpu_num >=0 && assign_cpu_num <=7) {
        _check_and_set_affinity();
    }
    else {
        printf("assigning to specific cpu disabled\n");
    }
#endif

////////////////////////////////////////////////////////////////////////////////
// Initialize spi, setup the pipe, and launch threads
////////////////////////////////////////////////////////////////////////////////


    if(en_i2c){

        // init the expander
        printf("attempting to init the io expander\n");
        io_expander_init();

        printf("attempting to open I2C CCI\n");
        if(modal_lepton_connect()){
            fprintf(stderr, "WARNING, FAILED TO START I2C CCI\n");
            fprintf(stderr, "Starting in SPI-ONLY mode\n");
            en_i2c = 0;
        }
        else{
            printf("Successfully connected to I2C CCI\n");
        }
    }


    if(en_i2c){
        int ret = 0;
        if(shutter_mode == SHUTTER_MODE_AUTO){
            ret = modal_lepton_set_ffc_auto();
        }
        else if(shutter_mode == SHUTTER_MODE_MANUAL){
            ret = modal_lepton_set_ffc_manual();
        }
        else if(shutter_mode == SHUTTER_MODE_FLOW){
            ret = modal_lepton_set_ffc_flow();
        }
        else{
            fprintf(stderr, "ERROR, unknown shutter mode %d\n", shutter_mode);
        }

        if(ret){
            fprintf(stderr, "WARNING, FAILED TO SET SHUTTER MODE OVER CCI\n");
            fprintf(stderr, "Starting in SPI-ONLY mode\n");
            en_i2c = 0;
        }

    }

    // start the parsing thread
    main_running=1;
    init_publisher();
    if(start_spi_parser()) _quit(-1);

////////////////////////////////////////////////////////////////////////////////
// Stop all the threads and do cleanup
////////////////////////////////////////////////////////////////////////////////
//
    printf("waiting for first frame from lepton to create MPA pipes\n");
    while(main_running){
        usleep(500000);

#ifdef PLATFORM_QRB5165

        // read ffc profile and update as necessary for flow more


        // decide if we should do a shutter in flow shutter mode
        if(en_i2c && (modal_lepton_get_current_shutter_mode()==SHUTTER_MODE_FLOW)){

            // get and set the profile


            if(autopilot_is_not_in_position_mode()){
                // put shutter back to normal now we are not in position mode
                modal_lepton_get_and_set_ffc_profile(0);
                if(modal_lepton_s_since_shutter()>flow_shutter_s){
                    printf("running shutter manually while waiting for PX4 position flight mode\n");
                    modal_lepton_run_ffc();
                }
            }
            else{
                // in the air and in position mode!! set the in-air shutter params
                modal_lepton_get_and_set_ffc_profile(1);
            }
        }

        if(en_i2c && !modal_lepton_has_set_shutter_mode_since_reset()){
            printf("waiting to reset shutter mode after reboot\n");
            usleep(3000000);
            modal_lepton_reset_shutter_mode();
        }

        if(pipe_server_get_num_clients(CH_LOG)>0){
            //get data here for the log
            lepton_log.timestamp_ns = rc_time_monotonic_ns();
            lepton_log.aux_temp = modal_lepton_get_aux_temperature_celsius();
            lepton_log.fpa_temp = modal_lepton_get_fpa_temperature_celsius();
            lepton_log.time_since_shutter = modal_lepton_s_since_shutter();
            lepton_log.reboot_cnt = global_reboot_cnt;
            lepton_log.sync_cnt = global_sync_cnt;

            pipe_server_write(CH_LOG, &lepton_log, sizeof(lepton_log));
        }


#endif
    }


    _quit(0);

    return 0;
}
