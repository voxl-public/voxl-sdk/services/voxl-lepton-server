

#ifndef MODAL_LEPTON_CCI_H
#define MODAL_LEPTON_CCI_H


#include "common.h"

int modal_lepton_connect(void);

int modal_lepton_run_ffc(void);

double modal_lepton_s_since_shutter(void);

int modal_lepton_reboot(void);

int modal_lepton_set_ffc_manual(void);

int modal_lepton_set_ffc_auto(void);

int modal_lepton_set_ffc_flow(void);

int modal_lepton_has_set_shutter_mode_since_reset(void);

shutter_mode_t modal_lepton_get_current_shutter_mode(void);

void modal_lepton_reset_shutter_mode(void);

int modal_lepton_get_and_set_ffc_profile(int en_fast);

float modal_lepton_get_aux_temperature_celsius(void);

float modal_lepton_get_fpa_temperature_celsius(void);

#endif
