/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <voxl_io.h>
#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include <rc_math/timestamp_filter.h>
#include <rc_math/other.h>
#include "config_file.h"
#include "common.h"
#include "parser.h"
#include "publisher.h"
#include "modal_lepton_cci.h"

// max attempts to resync before resetting
#define RESYNC_LIMIT  5


static pthread_t data_thread;
static int has_read_frame_since_sync = 0;
static int sync_count = RESYNC_LIMIT-3; // often need to reboot immediately on start
static uint8_t* raw_frame;
static rc_ts_filter_t tsf;

static int _bad_id_cnt = 0;

static int spi_fd;

static struct lepton_timing_packet lepton_timing;
static int _lepton_timing_idx = -1;

static int publish_timing(){
    if (_lepton_timing_idx == (TIMING_PACKET_CNT - 1)) {
        pipe_server_write(CH_TIMING, &lepton_timing, sizeof(lepton_timing));
    }

    return 0;
}

// try to sync the spi bus, if force_reboot is true or this function has been
// called many times without working
static int _sync_lepton(int force_reboot)
{
    global_sync_cnt++; //global sync counter
    sync_count++;
    if(en_debug) printf("Syncing lepton camera, sync count: %d\n", sync_count);

    // James' sync method on QRB
    // I've seen it resync after 5 sync attempts, but after that it's a lost cause
    if(en_i2c){
        if( force_reboot || (sync_count>=RESYNC_LIMIT && !has_read_frame_since_sync)){
            fprintf(stderr, "too many errrors and syncs without a valid frame, rebooting lepton\n");
            // takes some time to reboot so start the sync counter below 0 to give it more time
            // to sync after a reboot, 10 seems to be enough
            modal_lepton_reboot();
        }
    }

    // SPI driver auto releases the CS pin after 300ms so don't wait longer than that
    // datasheet says to wait >185ms
    usleep(190000);

    has_read_frame_since_sync = 0;

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Gather segments and generate image
// -----------------------------------------------------------------------------------------------------------------------------
static void *_data_thread_func(__attribute__((unused)) void* context){
    int64_t start_of_segment_1_ts = 0;
    int8_t current_segment;

    if(en_debug) printf("starting SPI reader thread\n");

    int discard_cnt = 0;
    int next_id = 0;
    
    int good_seg_cnt = 0;
    int bad_seg_cnt = 0;

    uint8_t *read_arr;
    int n_to_read = 60; //can go up to 99

    read_arr = (uint8_t*)malloc(VOSPI_PACKET_SIZE*n_to_read);

RESET_POINT:

    current_segment = 0;
    while(main_running){
        // read as quickly as possible from spi in a large number of packets
        // put the valid data in the proper place in the frame
        // get a timestamp when you first get a complete segment 1
        // publish data when you get a complete segment 4
        // resync when you get bad packets
        // reboot if several resyncs in a row do not work

        if (en_timing_msg) {
            _lepton_timing_idx += 1;
            if (_lepton_timing_idx >= TIMING_PACKET_CNT) {
                memset(&lepton_timing, 0, sizeof(lepton_timing));
                _lepton_timing_idx = 0;
            }

            lepton_timing.time_start_read_ns[_lepton_timing_idx] = rc_time_monotonic_ns();
        }

        int bytes_read = read(spi_fd, read_arr, VOSPI_PACKET_SIZE*n_to_read);
        
        if (en_timing_msg) {
            lepton_timing.time_stop_read_ns[_lepton_timing_idx] = rc_time_monotonic_ns();
        }

        // printf("time_diff: %ld\n", time_stop_read - time_start_read);

        if(bytes_read != VOSPI_PACKET_SIZE*n_to_read){
            fprintf(stderr, "SPI READ ERROR\n");
        }

        for (int ii=0; ii<n_to_read; ii++) {
            if ( (read_arr[ii*VOSPI_PACKET_SIZE] & 0x0f) == 0x0f) {
                discard_cnt++;
            }
            else {
                //this could be valid data
                int id_now = read_arr[ii*VOSPI_PACKET_SIZE + 1];

                if (id_now > 59) {
                    _bad_id_cnt++;
                    if (_bad_id_cnt % 100 == 0) {
                        printf("id number out of range, cnt: %d, %d\n", id_now, _bad_id_cnt);
                    }

                    if (_bad_id_cnt > 500) {
                        printf("bad id limit reached so syncing lepton, sync_count: %d\n", sync_count);
                        _bad_id_cnt = 0;
                        _sync_lepton(0);

                        if (en_timing_msg) {
                            lepton_timing.try_sync[_lepton_timing_idx] = 1;
                            publish_timing();
                        }
                        goto RESET_POINT;
                    }
                }
                else {
                    if (id_now == 20) {
                        int segment_number = (read_arr[ii*VOSPI_PACKET_SIZE] & 0xf0) >> 4;
                        // printf("segment number: %d, %d, %d\n", segment_number, good_seg_cnt, bad_seg_cnt);

                        if (segment_number < 0 || segment_number > 4) {
                            printf("segment number OOB: %d\n", segment_number);
                            current_segment = 0;
                        }
                        else if (segment_number == 0) {
                            current_segment = 0;
                        }
                        else if (current_segment == 0 && segment_number == 1) {
                            //got the start of a new frame
                            current_segment = 1;
                            start_of_segment_1_ts = rc_time_monotonic_ns() - READ_DELAY_NS;
                        }
                        else if (current_segment != segment_number) {
                            printf("WARNING Expected segment %d, received segment %d\n", current_segment, segment_number);
                            current_segment = 0;
                        }

                    }

                    if (next_id == id_now) {

                        //put the data in the proper place in the frame
                        
                        int idx_raw_frame_start;
                        if (id_now < 20 && current_segment == 0) {
                            //always add the first 19 packets to the start of the frame in case it is the start of the actual frame
                            idx_raw_frame_start = id_now*VOSPI_PACKET_SIZE;
                            memcpy(raw_frame + idx_raw_frame_start, read_arr + ii*VOSPI_PACKET_SIZE, VOSPI_PACKET_SIZE);
                        }
                        else if (current_segment != 0) {
                            idx_raw_frame_start = PACKETS_PER_SEGMENT*VOSPI_PACKET_SIZE*(current_segment - 1) + id_now*VOSPI_PACKET_SIZE;
                            memcpy(raw_frame + idx_raw_frame_start, read_arr + ii*VOSPI_PACKET_SIZE, VOSPI_PACKET_SIZE);
                        }

                        if (id_now == 59) {
                            if (current_segment == 4) {
                                if(en_debug){
                                    printf("Received a complete frame, publishing\n");
                                }

                                publish_async((char*)raw_frame, start_of_segment_1_ts);

                                if (en_timing_msg) {
                                    lepton_timing.published[_lepton_timing_idx] = 1;
                                }
                                current_segment = 0;

                                //reset sync logic 
                                has_read_frame_since_sync = 1;
                                sync_count = 0;
                                _bad_id_cnt = 0;
                            }
                            else if (current_segment > 0) {
                                current_segment++;
                            }

                            good_seg_cnt++;
                            next_id = 0;
                        }
                        else {
                            next_id++;
                        }
                    }
                    else if(next_id != 0) {
                        bad_seg_cnt++;
                        next_id = 0;
                    }
                }
            }
        }

        if (en_timing_msg) {
            publish_timing();
        }

    }

    return NULL;
}


int start_spi_parser(void)
{
    int ret;

    // initialize timestamp filter
    // note the default timeout tolerance is 100ms but the FLIR only publishes
    // at 113ms, this is okay because it sometimes drops frames and we want to
    // catch that
    rc_ts_filter_init(&tsf, ODR_HZ);
    if(en_debug_timing) tsf.en_debug_prints = 1;
    //tsf.en_debug_prints = 1;

    raw_frame = (uint8_t*)malloc(RAW_FRAME_SIZE);

    pthread_attr_t tattr;
    pthread_attr_init(&tattr);

    ret = voxl_spi_init(spi_bus, SPI_MODE_3, spi_speed);
    if(ret < 0){
        fprintf(stderr, "ERROR Cannot continue with error\n");
        return -1;
    }
    spi_fd = voxl_spi_get_fd(spi_bus);

    pthread_create(&data_thread, &tattr, _data_thread_func, NULL);
    return 0;
}

void stop_spi_parser(void)
{
    pthread_join(data_thread, NULL);
    return;
}

void inform_parser_of_reboot(void)
{
    // start sync counter negative so we don't trip the sync reset too soon
    // while waiting for lepton to start up
    sync_count = RESYNC_LIMIT-10;
    return;
}
